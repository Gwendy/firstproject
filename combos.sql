DECLARE

@xop CHAR(1),
@xUsuario_ID INT = NULL,
@xCuenta_ID int = NULL,
@xsta INT,
@xmsg VARCHAR(255)

	SET NOCOUNT ON;
	SET @xsta=1
	SET @xmsg='OK'


	SET @xUsuario_ID=2
	SET @xop = 'V'
	SET @xCuenta_ID = 5

    IF (@xop = 'C')  -- consulta cuentas
		BEGIN
--original JC			SELECT DISTINCT C.Cuenta_ID, Cuenta_Cve, C.Persona_ID, Per_NomPer 
			SELECT DISTINCT C.Cuenta_ID, Per_NomPer 
			FROM tbl_cuenta C, Tbl_persona P, tbl_PuntoVenta V, tbl_r_UsuarioPuntoVenta R, tbl_r_CampanaCuenta W 
			WHERE R.Usuario_ID    = @xUsuario_ID 
			AND R.PuntoVenta_ID = V.PuntoVenta_ID
			AND V.Cuenta_ID = C.Cuenta_ID
			AND V.cuenta_ID = W.cuenta_ID
			AND C.persona_ID = P.Persona_ID 
			ORDER BY Per_NomPer			
		END

    IF (@xop = 'P')  -- consulta polizas
		BEGIN
			SELECT Distinct V.VersionPoliza_CVE, V.VersionPoliza_ID 
			FROM tbl_versionPoliza V, tbl_r_campanacuenta R, tbl_poliza P 
			WHERE Cuenta_ID  = @xCuenta_ID
			AND P.CampanaCuenta_ID = R.CuentaCampana_ID 
			AND P.Poliza_ID = V.Poliza_ID 
			AND '201805' BETWEEN SUBSTRING(V.Pve_IniVig,1,6) AND SUBSTRING(V.Pve_FinVig,1,6);
		END

    IF (@xop = 'V')  -- consulta puntos de venta
		BEGIN
			SELECT P.PuntoVenta_ID, P.Pto_NomPto, P.PuntoVenta_CVE 
			FROM tbl_puntoVenta P, tbl_r_Usuariopuntoventa R 
			WHERE P.Cuenta_ID = @xCuenta_ID
			AND R.Usuario_ID = @xUsuario_ID
			AND P.PuntoVenta_ID = R.PuntoVenta_ID
		END