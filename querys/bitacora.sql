--usuid:	2
--usunom:	Rodolfo Valencia Celis
--ctaid:	6
--ctanom:	CAJA POLPULAR ROSARIO S.C. DE A.P. DE R.L. DE C.V.
--polid:	12
--polnom:	VGCRO001R1
--pvid:	6
--pvnom:	Rosario Matriz
--fpro:	201809
--camid:	3
--camnom:	Ahorro
--tpoarc:	4
--menu:	null


--select * from tbl_archivo
--order by arc_mespro desc

----Usuarios para la bitacora
--SELECT '' AS clave, ' Todos los Usuarios' AS descrip
--				UNION
--				SELECT U.Usuario_ID, REPLACE(U.Usr_NomUsr, ';', ' ')
--				FROM tbl_campana C, tbl_r_CampanaUsuario R, tbl_usuario U
--				WHERE R.Campana_ID  = 4
--				AND C.Campana_ID  = R.Campana_ID 
--				AND U.Usuario_ID  = R.Usuario_ID 
--				ORDER BY descrip--U.Usr_NomUsr

--select * from tbl_campana
--select * from tbl_puntoventa
--select * from tbl_usuario

--SELECT * FROM tbl_archivo
DECLARE
@xop char(1),
@xCampana_ID int = NULL,
@xPuntoVenta_ID int= NULL,
@xstatus varchar(20)= NULL,
@xusr varchar(16)= NULL,
@xfch varchar(21)= NULL,
@xarch varchar(256)

DECLARE
@xquery varchar(2000) = '',
@xqueryu varchar(200) = '',
@xquerys varchar(100) = ''

SET @xop='D'

set @xstatus='S'
set @xfch='201711'
set @xPuntoVenta_ID=3
set @xCampana_ID=2
set @xusr=null

--SET @xarch = '''fuente VC SN NICOLAS NOV 2017_PRUEBAS_1hoja.xlsx'',''fuente VC SN NICOLAS NOV 2017_PRUEBAS_1hoja.xlsx'',
--					''ExcelOpenXmlMultipleImageInsert.xlsx'', ''fuente VC SN NICOLAS NOV 2017_PRUEBAS_1hoja.xlsx'''
SET @xarch='fuente VC SN NICOLAS NOV 2017_recortado .xlsx'

	IF (@xop = 'E')
		BEGIN
			IF (@xusr IS NOT NULL)
				SET @xqueryu = '	AND (Arc_UsrCar = ' + convert(varchar, @xusr) + ' OR Arc_UsrVal = ' + convert(varchar, @xusr) + '  OR Arc_UsrCal = ' + convert(varchar, @xusr) + ' OR Arc_UsrEmi = ' + convert(varchar, @xusr) + ')'

			SELECT @xquerys = CASE
						WHEN @xstatus = 'P' THEN '   AND  Arc_EstArc NOT IN (''Emitido'', ''Borrado'') '
						WHEN @xstatus = 'T' THEN '   AND  Arc_EstArc NOT IN (''Borrado'') '
						WHEN @xstatus = 'S' THEN ''
						ELSE 'AND  Arc_EstArc IN (''' + @xstatus + ''')'
						END

			SET @xquery = 'SELECT a.Archivo_ID, a.tpoArchivo_ID, a.Campana_ID, a.VersionPoliza_ID, a.PuntoVenta_ID, 
				a.Arc_MesPro, a.Arc_ConMes, a.Arc_NomOri, a.Arc_NomSis, a.Arc_EstArc, a.Arc_HasErr, 
				a.Arc_UsrCar, REPLACE(ucar.Usr_NomUsr,'';'','' '') AS NomUsrCar, a.Arc_FecCar, 
				a.Arc_UsrVal, REPLACE(uval.Usr_NomUsr,'';'','' '') AS NomUsrVal, a.Arc_FecVal, 
				a.Arc_UsrCal, REPLACE(ucal.Usr_NomUsr,'';'','' '') AS NomUsrCal, a.Arc_FecCal, 
				a.Arc_UsrEmi, REPLACE(uemi.Usr_NomUsr,'';'','' '') AS NomUsrEmi, a.Arc_FecEmi, 
				a.Arc_AudUsr, a.Arc_AudFec
			FROM tbl_archivo AS a LEFT OUTER JOIN
				tbl_usuario AS uemi ON a.Arc_UsrEmi = uemi.Usuario_ID LEFT OUTER JOIN
				tbl_usuario AS ucal ON a.Arc_UsrCal = ucal.Usuario_ID LEFT OUTER JOIN
				tbl_usuario AS ucar ON a.Arc_UsrCar = ucar.Usuario_ID LEFT OUTER JOIN
				tbl_usuario AS uval ON a.Arc_UsrVal = uval.Usuario_ID
			WHERE Arc_MesPro    = ''' + @xfch + '''
				AND PuntoVenta_ID = ' + convert(varchar, @xPuntoVenta_ID) + '
				AND Campana_ID    = ' + convert(varchar, @xCampana_ID)
				+ @xqueryu
				+ @xquerys

			print @xquery
			exec(@xquery)
		END

		IF (@xop = 'D')
		BEGIN
			SELECT B.Bitacora_ID, B.Campana_ID, B.PuntoVenta_ID, B.Bit_NomArc, B.Bit_NomAsp, B.Bit_NomOpe, B.Bit_ResOpe,
				B.Bit_InfBit,  B.bit_AudUsr, B.Bit_FecAud, Cam_NomCam, Pto_NomPto
			FROM tbl_bitacora B, tbl_campana C, tbl_puntoventa P 
			WHERE Bit_NomArc IN (@xarch)
			AND C.Campana_ID = B.Campana_ID AND P.PuntoVenta_ID = B.PuntoVenta_ID 
			ORDER BY Bit_NomArc, Bit_FecAud
		END
/*
SELECT a.Archivo_ID, a.tpoArchivo_ID, a.Campana_ID, a.VersionPoliza_ID, a.PuntoVenta_ID, 
				a.Arc_MesPro, a.Arc_ConMes, a.Arc_NomOri, a.Arc_NomSis, a.Arc_EstArc, a.Arc_HasErr, 
				a.Arc_UsrCar, REPLACE(ucar.Usr_NomUsr,';',' ') AS NomUsrCar, a.Arc_FecCar, 
				a.Arc_UsrVal, REPLACE(uval.Usr_NomUsr,';',' ') AS NomUsrVal, a.Arc_FecVal, 
				a.Arc_UsrCal, REPLACE(ucal.Usr_NomUsr,';',' ') AS NomUsrCal, a.Arc_FecCal, 
				a.Arc_UsrEmi, REPLACE(uemi.Usr_NomUsr,';',' ') AS NomUsrEmi, a.Arc_FecEmi, 
				a.Arc_AudUsr, a.Arc_AudFec
			FROM tbl_archivo AS a LEFT OUTER JOIN
				tbl_usuario AS uemi ON a.Arc_UsrEmi = uemi.Usuario_ID LEFT OUTER JOIN
				tbl_usuario AS ucal ON a.Arc_UsrCal = ucal.Usuario_ID LEFT OUTER JOIN
				tbl_usuario AS ucar ON a.Arc_UsrCar = ucar.Usuario_ID LEFT OUTER JOIN
				tbl_usuario AS uval ON a.Arc_UsrVal = uval.Usuario_ID
			WHERE --Arc_MesPro    = '201806' AND 
			PuntoVenta_ID = 6
				AND Campana_ID    = 3

select * from tbl_archivo
select * from tbl_bitacora	 where bit_nomarc like 'fuente VC SN NICOLAS NOV 2017%' + + '.xlsx'

update tbl_bitacora
set bit_nomarc = 'fuente VC SN NICOLAS NOV 2017_recortado .xlsx'
where bit_nomarc like 'fuente VC SN NICOLAS NOV 2017%' + + '.xlsx'

select * from tbl_campana where campana_id=2
select * from tbl_puntoventa where puntoventa_id=3

select distinct bit_nomope from tbl_bitacora
*/